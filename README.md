# Application config related library

### Installing
---
Add following code to your "composer.json" file
```json
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/donbidon/lib-config"
        }
    ],
    "require": {
        "donbidon/lib-array": "dev-master"
    }
```
and run `composer update`.

### Usage
---
```php
<?php

require_once "/path/to/autoload";

$config = \donbidon\Lib\Config\Ini::parse(file_get_contents("./config.ini.php"), TRUE);
```
Ini-file example:
```ini
;<?php die; __halt_compiler();

[core]
log[method] = "StdOut"
log.source[] = 'Foo::method1()'
log.source[] = 'Foo::method2()'
```
