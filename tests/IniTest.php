<?php
/**
 * Ini class unit tests.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 * @version   {$Id}
 */

namespace donbidon\Lib\Config;

/**
 * Ini class unit tests.
 */
class IniTest extends \PHPUnit\Framework\TestCase
{
    /**
     * INI-string for testing
     *
     * @var string
     */
    protected static $iniString;

    /**
     * Tests parsing without sections.
     *
     * @return void
     * @covers donbidon\Lib\Ini::parse
     */
    public function testNoSections()
    {
        Ini::setSectionDelimiter('.');
        $iniString = <<< EOT
[core]
log[method] = "StdOut"
log.source[] = 'Foo::method1()'
log.source[] = 'Foo::method2()'
log.source.4 = 'Foo::method3()'
EOT;
        $actual = Ini::parse($iniString, FALSE);
        $expected = [
            'log' => [
                'method'   => 'StdOut',
                'source' => [
                    0 => 'Foo::method1()',
                    1 => 'Foo::method2()',
                    4 => 'Foo::method3()',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual, "Testing of parsing without sections failed");
    }

    /**
     * Tests parsing with sections.
     *
     * @return void
     * @covers donbidon\Lib\Ini::parse
     */
    public function testSections()
    {
        Ini::setSectionDelimiter('.');
        $iniString = <<< EOT
[core]
log[method] = "StdOut"
log.source[] = 'Foo::method1()'
log.source[] = 'Foo::method2()'
log.source.4 = 'Foo::method3()'
EOT;
        $actual = Ini::parse($iniString, TRUE);
        $expected = [
            'core' => [
                'log' => [
                    'method' => 'StdOut',
                    'source' => [
                        0 => 'Foo::method1()',
                        1 => 'Foo::method2()',
                        4 => 'Foo::method3()',
                    ],
                ],
            ],
        ];
        $this->assertEquals($expected, $actual, "Testing of parsing with sections failed");
    }

    /**
     * Tests parsing using other section delimiter.
     *
     * @return void
     * @covers donbidon\Lib\Ini::parse
     * @covers donbidon\Lib\Ini::parse
     */
    public function testSectionDelimiter()
    {
        Ini::setSectionDelimiter('_');
        $iniString = <<< EOT
[core]
log[method] = "StdOut"
log_source[] = 'Foo::method1()'
log_source[] = 'Foo::method2()'
log_source_4 = 'Foo::method3()'
EOT;
        $actual = Ini::parse($iniString, FALSE);
        $expected = [
            'log' => [
                'method'   => 'StdOut',
                'source' => [
                    0 => 'Foo::method1()',
                    1 => 'Foo::method2()',
                    4 => 'Foo::method3()',
                ],
            ],
        ];
        $this->assertEquals($expected, $actual, "Testing of parsing using other section delimiter failed");
    }
}
