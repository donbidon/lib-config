<?php
/**
 * Extended parse_ini_string(), nested sections support.
 *
 * @copyright  <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license    https://opensource.org/licenses/mit-license.php
 * @version    {$Id}
 */

namespace donbidon\Lib\Config;

/**
 * Extended parse_ini_string(), nested sections support.
 *
 * ```php
 * print_r(Ini::parse(file_get_contents("./ini.php"), TRUE));
 * ```
 * applied to the following ini-file
 * ```ini
 * ;<?php die; __halt_compiler();
 *
 * [core]
 * log[method] = "StdOut"
 * log.source[] = 'Foo::method1()'
 * log.source[] = 'Foo::method2()'
 * log.source.4 = 'Foo::method3()'
 * ```
 * will produce
 * ```
 * Array (
 *     [core] => Array (
 *         [log] => Array (
 *             [method] => StdOut
 *             [source] => Array (
 *                 [0] => Foo::method1()
 *                 [1] => Foo::method2()
 *                 [4] => Foo::method3()
 *             )
 *         )
 *     )
 * )
 *
 * @link   http://php.net/manual/en/function.parse-ini-string.php
 *         PHP documentation for parse_ini_string()
 * @static
 */
class Ini
{
    /**
     * Nested section delimiter
     *
     * @var string
     */
    protected static $delimiter = '.';

    /**
     * Sets nested sections delimiter
     *
     * @param  string $delimiter
     * @return void
     */
    public static function setSectionDelimiter($delimiter)
    {
        self::$delimiter = $delimiter;
    }

    /**
     * Parses a configuration string.
     *
     * @param  string   $string
     * @param  bool     $processSections
     * @param  int      $scannerMode
     * @return array|FALSE
     * @link   http://php.net/manual/en/function.parse-ini-string.php
     *         PHP documentation for parse_ini_string()
     */
    public static function parse($string, $processSections = FALSE, $scannerMode = INI_SCANNER_NORMAL)
    {
        $parsed = parse_ini_string($string, $processSections, $scannerMode);
        if (is_array($parsed)) {
            if ($processSections) {
                foreach (array_keys($parsed) as $section) {
                    self::parseSection($parsed[$section]);
                }
            } else {
                self::parseSection($parsed);
            }
        }

        return $parsed;
    }

    /**
     * Parses section.
     *
     * @param    mixed  &$parsed
     * @return   void
     * @internal
     */
    protected static function parseSection(&$parsed)
    {
        $sections = array_keys($parsed);
        foreach ($sections as $section) {
            if (FALSE !== strpos($section, self::$delimiter)) {
                $deeperSections = explode(self::$delimiter, $section);
                $nextSection = $deeperSections[0];
                $value = $parsed[$section];
                unset($parsed[$section]);
                if (isset($parsed[$nextSection]) ? !is_array($parsed[$nextSection]) : TRUE) {
                    $parsed[$nextSection] = [];
                }
                self::setSections($parsed, $deeperSections, $value);
            }
        }
    }

    /**
     * @param    array &$target
     * @param    array $deeperSections
     * @param    mixed $value
     * @return   void
     * @internal
     */
    protected static function setSections(&$target, $deeperSections, $value)
    {
        $section = $deeperSections[0];
        if (sizeof($deeperSections) > 1) {
            if (!isset($target[$section])) {
                $target[$section] = [];
            }
            self::setSections(
                $target[$section],
                array_slice($deeperSections, 1),
                $value
            );
        } else {
            if ('[]' == substr($section, -2)) {
                $section = substr($section, 0, -2);
                if (!isset($target[$section])) {
                    $target[$section] = [];
                }
                $target[$section][] = $value;
            }
            $target[$section] = $value;
        }
    }
}
